# SummerBytesCodingComp

This is my repository for my code entries for the Summer Bytes Coding Competition hosted by UAT. <br/>
(LINK: https://www.eventbrite.com/e/sum) <br/>
My two entries were a robot controlled via computer vision and a general-purpose classifier built from TensorFlow.