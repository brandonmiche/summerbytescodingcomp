"""
	This is the main file for my robot control program. It will run the vision
	processing elements as well as generate the commands for the motors.
	
	This program uses OpenCV for Python 3.6 on the Raspberry Pi.
	This program uses python-serial for serial communication.
	
	Author: Brandon Michelsen
	Date: 6/19/2019
"""
# Import necessary libraries
import cv2 as cv # OpenCV for vision processing
import numpy as np
from ser_control import Port # Serial port module (see ser_control.py)
from time import sleep

port = Port() # Create a new serial port

cap = cv.VideoCapture(0) # Create a new capture device

cmds = ['F', 'L', 'R', 'B', 'S'] # Commands for the motors

""" Function for driving the robot """
# coord is the coordinate to move to
# img is the frame of the video capture
def drive(coord, img):
	global port
	global cmds
	
	mid_x = int((img.shape[1])/2) # Find the middle of the window
	
	left_thresh = int(mid_x - 100) # Left threshold for moving forward
	right_thresh = int(mid_x + 100) # Right threshold for moving forward
	
	#cv.rectangle(img, (left_thresh, coord[1]), (right_thresh, coord[1]), (0, 0, 255), 2)
	#cv.imshow("Robo Control", img)
	
	if coord[0] <= left_thresh:
		port.send(cmds[2]) # Turn right
		print('left')
	elif coord[0] >= right_thresh:
		port.send(cmds[1]) # Turn left
		print('right')
	else:
		port.send(cmds[0]) # Move forward
		print('forward')

""" Function for finding the path to travel down """
def find_path():
	global cap
	
	while(cap.isOpened()): # While the video capture is available
		ret, frame = cap.read() # Read the video capture
		
		if ret == True:
		     # Blur the image to clean up noise
			gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
			gray = cv.GaussianBlur(gray, (5, 5), 0)
			
			# Threshold the image to find objects
			thresh = cv.threshold(gray, 45, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)[1]
			
			# Blur, erode, and dilate to clean up the image
			thresh = cv.medianBlur(thresh, 5)
			erode = cv.erode(thresh, (15, 15), iterations=1)
			dilate = cv.dilate(erode, (15, 15), iterations=1)
			
			# Find the contours of the objects
			cnts = cv.findContours(dilate.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
			
			# Get the contour with the maximum area
			max_cnt = max(cnts[1], key=cv.contourArea)	
			#cv.drawContours(frame, [max_cnt], -1, (255, 0, 0), 3)
			
			# Find the furthest point we can travel to on that contour
			furthest = tuple(max_cnt[max_cnt[:, :, 1].argmin()][0])
			
			#cv.circle(frame, furthest, 8, (0, 0, 255), -1)
			
			#cv.imshow("Robot Control", frame)
			print(frame.shape[1])
			
			# Drive to the furthest point
			drive(furthest, frame)
		    
		    # Close the window
			if cv.waitKey(1) & 0xFF == ord('q'):
				break
		else:
			break

""" Main function """
def main():
	global port
	global cmds
	
	port.open() # Open the serial port

	print(port.receive()) # Read in initial serial data
	find_path() # Begin running the algorithm
	
	#if str(port.receive()) != "Motor Fault":
	port.send(cmds[4]) # Shutoff the motors
	
	# Close the capture
	cap.release()
	cv.destroyAllWindows()

""" Run the main function """
if __name__ == "__main__":
	main()
