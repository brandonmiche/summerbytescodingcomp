# Python Computer Vision Robot Control
This directory contains the code for running the computer vision control algorithms for the robot.

The robo_control.py file contains the code for running the main program. This includes the vision processing functionality as
well as the motor command functionality.

The ser_control.py file contains a class wrapper for the python-serial module that is used for sending motor commands to the Arduino
motor controller.
# Python Modules
The version of Python used with this code is Python 3.6

The included Python modules are as follows:
- opencv-python - used for vision processing
- python-serial - used for sending serial data to the Arduino
- time - used for timing functions