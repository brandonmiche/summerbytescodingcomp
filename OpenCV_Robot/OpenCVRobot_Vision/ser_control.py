"""
	This file creates the class used for serial communication. It uses 
	the python-serial module to communicate with an Arduino.
	
	Author: Brandon Michelsen
	Date: 6/19/2019
"""
# Import necessary libraries
import serial
import serial.tools.list_ports
from time import sleep

# Create the class
class Port:
	def __init__(self, baud=115200):
		self.ser = serial.Serial(timeout=5, rtscts=False, dsrdtr=False)
		
		self.ser.baudrate = baud # Set the baud rate
		
		ports = list(serial.tools.list_ports.comports())
		
		# Find all the Arduino ports
		for p in ports:
			if "Arduino" in p[1]:
				print("Arduino found on port", p[0])
				self.ser.port = p[0]
				break
		if self.ser.port == None:
			# If no Arduino ports found, tell the user
			print("No Arduino found")
			
	def __del__(self):
		self.close()
			
	def close(self):
		if self.ser.is_open == True:
			self.ser.close()
			
	def open(self):
		if self.ser.is_open == False and self.ser.port != None:
			self.ser.open()
			sleep(0.5)
			
	def send(self, data):
		if self.ser.is_open == True and self.ser.port != None:
			self.ser.write(data.encode()) # Send data through the serial port
			print(ord(data))
			sleep(0.5)
		else:
			print("Serial port not connected")
			
	def receive(self):
		if self.ser.is_open == True and self.ser.port != None:
			return (self.ser.read_until('\n', 32)).decode() # Get data from the serial port
		else:
			print("Serial port not connected")
			return None
