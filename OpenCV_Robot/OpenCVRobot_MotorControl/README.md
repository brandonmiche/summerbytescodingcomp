# Arduino Motor Controller Code
This directory stores the motor controller code for the OpenCV robot.

The Arduino receives movement commands from the Raspberry Pi and moves its
motors based on those commands. All commands are received through the Arduino's hardware serial.
# Command Structure
COMMANDS:<br/>
- 'F' = Move both motors forward relative to the robot
- 'B' = Move both motors backward relative to the robot
- 'R' = Turn right (move one set forward one set back)
- 'L' = Turn left (move one set forward one set back)

All commands are sent to the Arduino via its hardware serial lines. The commands are non-blocking, 
meaning that the Arduino will be able to continue to move after receving a command.<br/>
# Hardware
The hardware used in this device include:<br/>
- The Arduino Uno
- The Raspberry Pi 3
- Pololu Dual MC33926 Motor Driver

The Raspberry Pi is also powered from a Pololu S7V7F5 5V Step Up/Step Down Voltage regulator.