/*
 * This is the motor controller code for the computer-vision controlled robot. The controller interfaces with a Raspberry Pi 3 via serial
 * to receive motor commands. It then processes those motor commands to a drive signal. The drive signal moves the motors according to the
 * given command (i.e. 'F', 'B', 'R', 'L', and 'S').
 * 
 * COMMANDS:
 *    'F' = Drive forward
 *    'B' = Drive backward
 *    'R' = Stear right
 *    'L' = Stear left
 *    'S' = Full stop
 * 
 * The motors are driven using the Pololu Dual MC33926 Motor Shield.
 * 
 * Author: Brandon Michelsen
 * Date: 6/18/2019
 * 
 */

/* Include Libraries */
#include <DualMC33926MotorShield.h>

/* Function Prototypes */
void motorFaultStop(); // Function for detecting motor faults

/* Global Variables */
DualMC33926MotorShield motorDriver;

// Setup
void setup() {
  // Start serial
  Serial.begin(115200);
  Serial.println("OpenCV Robot Motor Controller");

  pinMode(LED_BUILTIN, OUTPUT);
  // Initialise the motor driver
  motorDriver.init();
}

// Contiuous Loop
void loop() {
  /* Local Variables */
  static char cmd = 'S'; // The command variable for the robot (initialise to 'S')

  static int16_t m1 = 0; // Speed command for motor 1 (initialise to 0)
  static int16_t m2 = 0; // Speed command for motor 2 (initialise to 0)

  // Read in the new command if it is availble
  if (Serial.available() > 0) {
    cmd = Serial.read();
    Serial.flush();
  }

  // Blink the onboard LED to show that we are running
  digitalWrite(LED_BUILTIN, HIGH);
  delay(20);
  digitalWrite(LED_BUILTIN, LOW);
  delay(20);

  // Map motor commands to motor signals
  m1 = cmd == 'F' || cmd == 'L' ? -300 : cmd == 'B' || cmd == 'R' ? 300 : 0;
  m2 = cmd == 'F' || cmd == 'R' ? 300 : cmd == 'B' || cmd == 'L' ? -300 : 0;
  motorDriver.setSpeeds(m1, m2);

  // Stop the motor if there is a fault
  motorFaultStop();
}

// Function for detecting motor faults
void motorFaultStop() {
  if (motorDriver.getFault()) {
    Serial.println("Motor Fault");
    while(1);
  }
}