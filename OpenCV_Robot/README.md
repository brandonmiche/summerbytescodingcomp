# Computer Vision Robot with OpenCV
This directory contains all the code for my computer vision controlled robot using OpenCV. It
uses a Raspberry Pi for processing vision data from a webcam and then sends commands based on 
the processed data to an Arduino. The Arduino will then drive motors connected to a frame based
on those commands.

This is my first entry for the Summer Bytes Coding Competition hosted by UAT (see link in the above directory)